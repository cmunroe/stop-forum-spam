<?php


spl_autoload_register(function ($class_name) {
    $class_name = str_replace('\\', '/', $class_name);

    //echo "src/" . $class_name . '.php' . "\n";
    include "src/" . $class_name . '.php';
});

// Class Endpoints.
$core = new stopForumSpam\core;
$listed = new stopForumSpam\listed;
$caching = new stopForumSpam\caching;
$emails = new stopForumSpam\listed\emails;
$ips = new stopForumSpam\listed\ips;
$ipv4 = new stopForumSpam\listed\ipv4;
$ipv6 = new stopForumSpam\listed\ipv6;
$misc = new stopForumSpam\misc;
$toxic = new stopForumSpam\toxic;
$toxic_ips = new stopForumSpam\toxic\ips;
$toxic_domains = new stopForumSpam\toxic\domains;
$toxic_usernames = new stopForumSpam\toxic\usernames;
$usernames = new stopForumSpam\listed\usernames;
