var links = document.links;
var downloads = [];

for(var i = 0; i < links.length; i++){

    var md5 = false;
    var age = 1;

    if(links[i].href.includes('/downloads/')){

        if(links[i].href.includes('.md5')){

            continue;

        }

        if(links[i].href.includes('.zip') || links[i].href.includes('.gz') ){

            md5 = true;

        }

        if(links[i].href.includes('listed_') && !links[i].href.includes('_1_') && !links[i].href.includes('_1.')){

            age = 12;

        }
        if(links[i].href.includes('bannedips.zip') || links[i].href.includes('spamdomains.zip')){

            md5 = false;
            age = 12;

        }

        var file = links[i].href.replace('https://www.stopforumspam.com/downloads/', '');

        downloads.push({'age':age, 'file':file, 'md5':md5});

    }
    
}

JSON.stringify(downloads, null, 4);
