<?php

namespace stopForumSpam;

class caching extends core
{

    /**
     * Update the cache at the cache_location.
     *
     * @return boolean false if cache isn't set.
     */
    public function get_cache(){

        // Make sure our cache is set.
        if(!$this->cache_set == true){
            
            return false;

        }

        // Get our config.
        $files = json_decode(file_get_contents(__DIR__ . '/caching_files.json'), true);

        // Get our Cache info.
        $cache_info = $this->cache_info();

        // Loop through our files.
        foreach($files as $file){

            // Determine if it is in the cache_info.
            if(array_key_exists($file['file'], $cache_info)){

                // Check to see if it expired.
                if($cache_info[$file['file']]['expires'] < time()){

                    // Grab it.
                    $this->get_cache_checker($file['file'], $file['age'], $file['md5']);

                }

            }

            // Not in the cache.
            else{

                // Grab it.
                $this->get_cache_checker($file['file'], $file['age'], $file['md5']);

            }

        }

    }

    /**
     * Function to determine if the specific file needs to be updated
     * against md5 has checks.
     *
     * @param string $file // Filename
     * @param integer $age // Age before expired
     * @param boolean $md5 // if md5 is set for this file.
     * @return void
     */
    protected function get_cache_checker(string $file, int $age, bool $md5){

        // Get our Cache info.
        $cache_info = $this->cache_info();

        $md5_hash_current = null;

        // If a MD5 supported file.
        if($md5 == true){

            $md5_hash_current = $this->http_get($this->static_url . $file . ".md5");

            if(
                !array_key_exists($file, $cache_info) ||
                !array_key_exists('md5', $cache_info[$file]) || 
                $cache_info[$file]['md5'] !== $md5_hash_current
            ){

                // Update our file.
                $this->get_cache_updater( $file, $age, $md5_hash_current);

            }

        }

        else{

            // Update our file.
            $this->get_cache_updater($file, $age, $md5_hash_current);

        }

    }

    /**
     * Cache Updater. 
     * 
     * Updates the currently held cache files.
     *
     * @param string $file // Filename we want.
     * @param integer $age // age of the file before expire.
     * @param string $md5 // md5 hash.
     * @return void
     */
    protected function get_cache_updater(string $file, int $age, string $md5 = null){

        // Get our Cache info.
        $cache_info = $this->cache_info();

        // Get the newest file.
        $newFile = $this->http_get($this->static_url .  $file);

        if($newFile !== false){

            // Update our file.

            // Put the file.
            file_put_contents($this->cache_url . $file, $newFile);

            if($md5 != null){

                file_put_contents($this->cache_url . $file . ".md5", $md5);

            }

            // Edit Cache Values.
            $cache_info[$file]['md5'] = $md5;
            $cache_info[$file]['expires'] = $age * 60 * 60 + time();
            $cache_info[$file]['time'] = time();

            // Put our cache.
            file_put_contents($this->cache_url . 'cache_info.json', json_encode($cache_info, JSON_PRETTY_PRINT));
            
        }

    }

}