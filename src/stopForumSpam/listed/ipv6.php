<?php

namespace stopForumSpam\listed;
use stopForumSpam\listed;

class ipv6 extends listed
{
    
    /**
     * Get an array of IPv6 addresses from Stop Forum Spam.
     *
     * @param integer $length $this->valid_listed_ages; has a list of valid ages.
     * @return array list of ips.
     * array( 
     *     [..]
     *     [22] => 2800:810:44f:82d1::
     *     [23] => 2a02:40:852:ffff::
     *     [24] => 2a02:908:4c6:a2a0::
     *     [25] => 2a02:c7f:7232::
     *     [26] => 2a02:c207:2011:9696::
     * )
     * 
     */
    public function get(int $length){

        // Validate our length.
        if(!in_array($length, $this->valid_listed_ages)){

            return false;

        }

        // Get the file.
        $file = $this->get_wrapper('listed_ip_' . $length . "_ipv6.gz");

        // Decompress.
        $file = $this->decompress($file);

        // Parser.
        $file = $this->parser($file);

        // return file.
        return $file;

    }

    /**
     * Get an array of IPv6 addresses from Stop Forum Spam.
     *
     * @param integer $length $this->valid_listed_ages; has a list of valid ages.
     * @return array list of ips.
     * 
     * array(
     *    [..]
     *    [24] => Array
     *        (
     *            [ips] => "2a02:908:4c6:a2a0::"
     *            [reportedCount] => "3"
     *            [lastSeenUTC] => "2019-09-19 18:58:30"
     *        )
     *
     *    [25] => Array
     *        (
     *            [ips] => "2a02:c7f:7232::"
     *            [reportedCount] => "1"
     *            [lastSeenUTC] => "2019-09-20 02:58:11"
     *        )
     *
     *    [26] => Array
     *        (
     *            [ips] => "2a02:c207:2011:9696::"
     *            [reportedCount] => "32"
     *            [lastSeenUTC] => "2019-09-19 15:38:53"
     *        )
     * )
     *
     */
    public function get_all(int $length){

        // Validate our length.
        if(!in_array($length, $this->valid_listed_ages)){

            return false;

        }

        // Get the file.
        $file = $this->get_wrapper('listed_ip_' . $length . "_ipv6_all.gz");

        // Decompress.
        $file = $this->decompress($file);

        // Parser.
        $file = $this->parser_all($file, 'ips');

        // return file.
        return $file;

    }
 
}