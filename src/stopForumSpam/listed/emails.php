<?php

namespace stopForumSpam\listed;
use stopForumSpam\listed;

class emails extends listed
{

    /**
     * Get an array of email addresses from Stop Forum Spam.
     *
     * @param integer $length $this->valid_listed_ages; has a list of valid ages.
     * @return array list of emails.
     * array( 
     *     [....]
     *     [6694] => zurmkidcciu@gmail.com
     *     [6695] => zurn@harrow-plumber.co.uk
     *     [6696] => zv2@yuji010.masumi64.kiesag.xyz
     *     [6697] => zverevcheslav47@mail.ru
     *     [6698] => zx18@masato7510.takayuki98.dev256.xyz
     *     [6699] => zzrmkynbq@botseo.ru
     * )
     * 
     */
    public function get(int $length){

        // Validate our length.
        if(!in_array($length, $this->valid_listed_ages)){

            return false;

        }

        // Get the file.
        $file = $this->get_wrapper('listed_email_' . $length . ".gz");

        // Decompress.
        $file = $this->decompress($file);

        // Parser.
        $file = $this->parser($file);

        // return file.
        return $file;

    }

    /**
     * Get an array of email addresses from Stop Forum Spam.
     *
     * @param integer $length $this->valid_listed_ages; has a list of valid ages.
     * @return array list of emails.
     * 
     * array(
     *    [....]
     *    [6696] => Array
     *        (
     *            [email] => "zverevcheslav47@mail.ru"
     *            [reportedCount] => "10"
     *            [lastSeenUTC] => "2019-09-19 11:34:16"
     *        )
     *
     *    [6697] => Array
     *        (
     *            [email] => "zx18@masato7510.takayuki98.dev256.xyz"
     *            [reportedCount] => "2"
     *            [lastSeenUTC] => "2019-09-19 09:36:36"
     *        )
     *
     *    [6698] => Array
     *        (
     *            [email] => "zzrmkynbq@botseo.ru"
     *            [reportedCount] => "5"
     *            [lastSeenUTC] => "2019-09-19 03:03:37"
     *        )
     *
     *  )
     */
    public function get_all(int $length){

        // Validate our length.
        if(!in_array($length, $this->valid_listed_ages)){

            return false;

        }

        // Get the file.
        $file = $this->get_wrapper('listed_email_' . $length . "_all.gz");

        // Decompress.
        $file = $this->decompress($file);

        // Parser.
        $file = $this->parser_all($file, 'email');

        // return file.
        return $file;

    }
 
}