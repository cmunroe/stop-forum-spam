<?php

namespace stopForumSpam\listed;
use stopForumSpam\listed;

class usernames extends listed
{

    /**
     * Get an array of usernames from Stop Forum Spam.
     *
     * @param integer $length $this->valid_listed_ages; has a list of valid ages.
     * @return array list of usernames.
     * array( 
     *     [....]
     *     [9898] => ช่วยตัวเอง
     *     [9899] => เงี่ยน
     *     [9900] => เย็ดกัน
     *     [9901] => 강남오피
     *     [9902] => 대구오피
     *     [9903] => 먹튀검증
     *     [9904] => 먹튀검증업체
     * )
     * 
     */
    public function get(int $length){

        // Validate our length.
        if(!in_array($length, $this->valid_listed_ages)){

            return false;

        }

        // Get the file.
        $file = $this->get_wrapper('listed_username_' . $length . ".gz");

        // Decompress.
        $file = $this->decompress($file);

        // Parser.
        $file = $this->parser($file);

        // return file.
        return $file;

    }

    /**
     * Get an array of usernames from Stop Forum Spam.
     *
     * @param integer $length $this->valid_listed_ages; has a list of valid ages.
     * @return array list of usernames.
     * 
     * array(
     *     [....]
     *     [9901] => Array
     *         (
     *             [username] => "대구오피"
     *             [reportedCount] => "1"
     *             [lastSeenUTC] => "2019-09-19 17:34:12"
     *         )
     * 
     *     [9902] => Array
     *         (
     *             [username] => "먹튀검증"
     *             [reportedCount] => "2"
     *             [lastSeenUTC] => "2019-09-19 17:35:36"
     *         )
     * 
     *     [9903] => Array
     *         (
     *             [username] => "먹튀검증업체"
     *             [reportedCount] => "3"
     *             [lastSeenUTC] => "2019-09-19 17:37:06"
     *         )
     *
     *  )
     */
    public function get_all(int $length){

        // Validate our length.
        if(!in_array($length, $this->valid_listed_ages)){

            return false;

        }

        // Get the file.
        $file = $this->get_wrapper('listed_username_' . $length . "_all.gz");

        // Decompress.
        $file = $this->decompress($file);

        // Parser.
        $file = $this->parser_all($file, 'username');

        // return file.
        return $file;

    }

}