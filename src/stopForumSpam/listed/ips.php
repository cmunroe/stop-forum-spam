<?php

namespace stopForumSpam\listed;
use stopForumSpam\listed;

class ips extends listed
{

    /**
     * Get an array of IPv4 and IPv6 addresses from Stop Forum Spam.
     *
     * @param integer $length $this->valid_listed_ages; has a list of valid ages.
     * @return array list of ips.
     * array( 
     *     [....]
     *     [4992] => 223.206.236.175
     *     [4993] => 223.206.237.107
     *     [4994] => 223.206.238.84
     *     [4995] => 223.206.241.190
     *     [4996] => 223.206.242.182
     *     [4997] => 223.233.92.228
     *     [4998] => 223.237.192.118
     * )
     * 
     */
    public function get(int $length){

        // Validate our length.
        if(!in_array($length, $this->valid_listed_ages)){

            return false;

        }

        // Get the file.
        $file = $this->get_wrapper('listed_ip_' . $length . "_ipv46.gz");

        // Decompress.
        $file = $this->decompress($file);

        // Parser.
        $file = $this->parser($file);

        // return file.
        return $file;

    }

    /**
     * Get an array of IPv4 and IPv6 addresses from Stop Forum Spam.
     *
     * @param integer $length $this->valid_listed_ages; has a list of valid ages.
     * @return array list of ips.
     * 
     * array(
     *    [....]
     *    [6849] => Array
     *        (
     *            [ips] => "223.206.237.175"
     *            [reportedCount] => "35"
     *            [lastSeenUTC] => "2019-09-17 01:53:17"
     *        )
     *
     *    [6850] => Array
     *        (
     *            [ips] => "223.206.241.190"
     *            [reportedCount] => "29"
     *            [lastSeenUTC] => "2019-09-17 12:52:26"
     *        )
     *
     *    [6851] => Array
     *        (
     *            [ips] => "223.233.87.184"
     *            [reportedCount] => "2"
     *            [lastSeenUTC] => "2019-09-16 06:51:42"
     *        )
     *  )
     */
    public function get_all(int $length){

        // Validate our length.
        if(!in_array($length, $this->valid_listed_ages)){

            return false;

        }

        // Get the file.
        $file = $this->get_wrapper('listed_ip_' . $length . "_ipv46_all.gz");

        // Decompress.
        $file = $this->decompress($file);

        // Parser.
        $file = $this->parser_all($file, 'ips');

        // return file.
        return $file;

    }
    
}