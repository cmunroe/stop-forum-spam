<?php

namespace stopForumSpam;

class listed extends core
{

    /**
     * Valid ages for listed.
     *
     * @var array valid ages.
     */
    public      $valid_listed_ages = array(1, 7, 30, 90, 180, 365);

}