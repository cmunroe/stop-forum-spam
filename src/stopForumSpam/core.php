<?php

namespace stopForumSpam;

class core
{

    /** 
     * Site to download the original copies from.
     * 
     * @var string url.
     */
    protected     $static_url = 'http://www.stopforumspam.com/downloads/';

    /**
     * Whether or not the cache has been set.
     *
     * @var boolean 
     */
    public      $cache_set = false;

    /**
     * The path to the cache.
     *
     * @var string cache url/path.
     */
    public      $cache_url = null;


    /**
     * Grab our file.
     * 
     * Uses the cache if possible, if not uses the static method.
     *
     * @param string $file name that you wish to get.
     * @return string file. (( Compressed most likely. ))
     */
    public function get_wrapper(string $file){

        // See if our cache is set.
        if($this->cache_set == true){

            $url = $this->cache_url . $file;

        }

        // If the cache isn't set, use the static_url. 
        else{

            $url = $this->static_url . $file;

        }

        // Check if this is a HTTP request.
        if(strpos($url, 'http://') !== false || strpos($url, 'https://') !== false){

            return $this->http_get($url);

        }

        // Must be a local file cache.
        else if(file_exists($url)) {

            return file_get_contents($url);

        }

    }

    /**
     * Set our cache.
     *
     * @param string $url to cache.
     * @return this chain.
     */
    public function cache_set(string $url){

        // Set our cache to true.
        $this->cache_set = true;

        // Place a / at the end of the string if needed.
        if(substr($url, -1) != "/"){

            $url .= "/";

        }

        // Set our cache url.
        $this->cache_url = $url;

        // support chaning.
        return $this;

    }

    /**
     * Download a file from the interwebs.
     *
     * @param string $url to the resouce.
     * @return string document
     * @return bool false if download failed.
     */
    public function http_get(string $url){

        // Get our file.
        $response = file_get_contents($url);

        // Verify we got a valid response.
        if(!strpos($http_response_header[0], "200")){

            return false;

        }

        // Return data.
        return $response;

    }

    /**
     * Stop Forum Spam Parser for *_all.txt files.
     *
     * @param string $fileData The uncompressed data.
     * @param string $dataKey The key for this file type. (Email | IP | etc.)
     * @return array formatted multidimensional array of the parsed data.
     */
    public function parser_all(string $fileData, string $dataKey){

        // Storage array
        $formattedList = array();

        // Explode the file by line.
        $fileData = explode(PHP_EOL, $fileData);

        foreach($fileData as $line){

            // explode by commas.
            $line = explode(",", $line);

            // Verify we have a good list.
            if(
                array_key_exists(0, $line) &&
                $line[0] != null 
            ){

                // Insert into formatted list.
                $formattedList[] = array(
                    $dataKey => $line[0],
                    'reportedCount' => $line[1],
                    'lastSeenUTC' => $line[2]
                );
            
            }
            
        }
        
        // Return Data.
        return $formattedList;

    }

    /**
     * Parser for regular files.
     *
     * @param string $fileData uncompressed.
     * @return array list of data.
     */
    public function parser(string $fileData){


        // Explode the file by line.
        $fileData = explode(PHP_EOL, $fileData);

        foreach($fileData as $key => $line){

            // Clean up null lines.
            if($line == null){

                unset($fileData[$key]);

            }

        }

        // Return the data.
        return $fileData;

    }

    /**
     * Returns an array of the cache status and details.
     *
     * @return array of cache details.
     * 
     * @return boolean false if cache is not set, or cache_info isn't found.
     */
    public function cache_info(){

        // Check to see if the cache is set.
        if(!$this->cache_set == true){

            return false;

        }

        // Grab the file from the cache.
        $response = file_get_contents($this->cache_url . 'cache_info.json');

        // Check to make sure the response is set.
        if($response == null){

            return array();

        }

        // Convert to an array.
        $response = json_decode($response, true);

        // return.
        return $response;

    }


    /**
     * Decompress the .gz files from stop forum spam.
     *
     * @param string $string  compressed string
     * @return string uncompressed.
     */
    public function decompress(string $string){

        return gzdecode($string);

    }

}