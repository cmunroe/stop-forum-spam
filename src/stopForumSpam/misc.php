<?php

namespace stopForumSpam;


class misc extends core
{

/**
     * Valid Types.
     *
     * @var array of valid types.
     */
    public      $valid_types = array('toxic', 'listed');

    /**
     * Valid types for listed_ip_
     *
     * @var array
     */
    public      $valid_ip_types = array('ipv6', 'ipv46', '' , 'all', 'ipv6_all', 'ipv46_all');

    /**
     * Valid Listed types.
     *
     * @var array
     */
    public      $valid_listed = array('email', 'username', 'ip');

    /**
     * Valid listed extension formats.
     *
     * @var array
     */
    public      $valid_listed_extensions = array('.zip', '.gz', '.zip.md5' , '.gz.md5');

    /**
     * A list of valid toxic types.
     *
     * @var array
     */
    public      $valid_toxics = array('ip_cidr', 'ip_range', 'domains_whole', 'domains_partial', 'username_partial', 'domains_whole_filtered');


    /**
     * Path Builder (( Probably Not Needed.))
     *
     * @param string $extension
     * @param string $type
     * @param string $subtype
     * @param intiger $length
     * @param string $specialtype
     * @return void
     */
    public function path_builder(
        string $extension, 
        string $type, 
        string $subtype, 
        $length = null, 
        string $specialtype = null
        ){

        // Check if our type is valid.
        if(!array_key_exists($type, $this->valid_types)){

            return false;

        }

        // Create the first step in our path.
        $path = $type . "_";

        // ============= Listed ==================
        if($type == 'listed'){

            // Set our length if it isn't set.
            if($length == null){

                $length = 1;

            }

            // Check if our subtype and length is valid.
            if(
                !array_key_exists($subtype, $this->valid_listed) || 
                !array_key_exists($length, $this->valid_listed_ages) || 
                !array_key_exists($extension, $this->valid_listed_extensions
            )){

                return false;

            }

            // set our subtype to the path.
            $path .= $subtype . "_" . $length;

            // Check against our special type.
            if($specialtype != null && array_key_exists($specialtype, $this->valid_ip_types)){

                $path .= "_" . $specialtype;

            }

            // Set our extension.
            $path .= $extension;
            
            return $path;

        }

        // ============= Toxic ==================
        if($type == 'toxic'){

            // Check if our subtype is valid.
            if(
                !array_key_exists($subtype, $this->valid_toxics) ||
                $extension != '.txt'
            ){

                return false;

            }

            // set our subtype to the path.
            $path .= $subtype . "_";

            // Check and set our age if needed.
            if(
                $subtype = 'domains_whole_filtered' && 
                $length != null && 
                array_key_exists($length, $this->valid_toxic_ages)
                ){

                $path .= "_" . $length;


            }

            // Set extension
            $path .= $extension;

            return $path;
            
        }

        // catch all.

        return false;

    }

    /**
     * Where are we storing the cache.
     *
     * @var string path to where you want to store the cache.
     */
    public      $cache_destination = null;

    /**
     * Set the Cache Destination
     *
     * @param sting $filePath
     * @return void
     */
    public function cache_dest(sting $filePath){

        // Set our destination
        $this->cache_destination = $filePath;

        return $this;

    }

}