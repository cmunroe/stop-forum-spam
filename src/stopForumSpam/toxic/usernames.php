<?php

namespace stopForumSpam\toxic;
use stopForumSpam\toxic;

class usernames extends toxic
{
    /**
     * Return the raw file.
     *
     * @return string
     * 
     * [...]
     * cheapshoes
     * chongsoft
     * cloutletonline
     * mont blanc
     * xrumer888
     * [...]
     */
    public function raw(){

        // Get our File.
        $file = $this->get_wrapper('toxic_usernames_partial.txt');

        // Return our File.
        return $file;

    }

    /**
     * Get a parsed list of toxic usernames form stop forum spam.
     *
     * @return array
     * 
     * Array
     * (
     *     [0] => cheapshoes
     *     [1] => chongsoft
     *     [2] => cloutletonline
     *     [3] => mont blanc
     *     [4] => xrumer888
     * )
     */
    public function get(){

        // Get our file.
        $file = $this->raw();

        // Parse it.
        $file = $this->parser($file);

        // Return.
        return $file;

    }

}