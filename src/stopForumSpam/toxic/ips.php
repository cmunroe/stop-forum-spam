<?php

namespace stopForumSpam\toxic;
use stopForumSpam\toxic;

class ips extends toxic
{
    /**
     * Get the toxic range file from Stop Forum Spam.
     *
     * @return string raw file.
     * [...]
     * 109.200.1.0-109.200.31.255
     * 146.185.223.0-146.185.223.255
     * [...]
     */
    public function range_raw(){

        // Get our File.
        $file = $this->get_wrapper('toxic_ip_range.txt');

        // Return our File.
        return $file;

    }

    /**
     * Get the toxic range file from Stop Forum Spam in a parsed format.
     *
     * @return array
     * array(
     *     [..] 
     *     [15] => Array
     *        (
     *            [start] => 91.210.104.0
     *            [end] => 91.210.107.255
     *        )
     *
     *    [16] => Array
     *        (
     *            [start] => 91.236.74.0
     *            [end] => 91.236.75.255
     *        )
     *
     *)
     */
    public function range(){

        // Get our File.
        $file = $this->range_raw();

        // Parse the file into lines.
        $file = $this->parser($file);

        // Storage Array
        $response = array();

        // split into start and end.
        foreach($file as $range){

            $split = explode('-', $range);

            $response[] = array(
                'start' => $split[0],
                'end' => $split[1]
            );

        }

        return $response;

    }

    /**
     * Grab the file raw from Stop Forum Spam.
     *
     * @return string
     * 
     * [...]
     * 109.200.1.0/24
     * 109.200.2.0/23
     * 109.200.4.0/22
     * 109.200.8.0/21
     * [...]
     */
    public function cidr_raw(){

        // Get our File.
        $file = $this->get_wrapper('toxic_ip_cidr.txt');

        // Return our File.
        return $file;

    }

    /**
     * Parse the cidr into an array.
     *
     * @return array
     * array(
     *     [..]
     *     [38] => Array
     *         (
     *             [ipRange] => 91.210.104.0
     *             [subnetMask] => 22
     *         )
     * 
     *     [39] => Array
     *         (
     *             [ipRange] => 91.236.74.0
     *             [subnetMask] => 23
     *         )
     * 
     * )
     */
    public function cidr(){

        // Get our File.
        $file = $this->cidr_raw();

        // Parse the file into lines.
        $file = $this->parser($file);

        // Storage Array
        $response = array();

        // split into start and end.
        foreach($file as $range){

            $split = explode('/', $range);

            $response[] = array(
                'ipRange' => $split[0],
                'subnetMask' => $split[1]
            );

        }

        return $response;

    }

}