<?php

namespace stopForumSpam\toxic;
use stopForumSpam\toxic;

class domains extends toxic
{

    /**
     * A list of valid toxic_domains .. _filtered types.
     *
     * @var array
     */
    public      $valid_toxic_ages = array(1000, 10000, 50000, 100000, 250000);

    /**
     * Returns the raw filtered domain list file.
     *
     * @param integer $length based upon $valid_toxic_ages
     * @return string
     */
    public function filtered_raw(int $length = null){

        // Make sure we have a valid length.
        if($length != null && !in_array($length, $this->valid_toxic_ages)){

            return false;

        }

        // Generate the filename.
        $fileName = 'toxic_domains_whole_filtered';

        if($length != null){
            
            $fileName .= "_" . $length;
        }

        $fileName .= '.txt';

        // Get our File.
        $file = $this->get_wrapper($fileName);

        return $file;

    }

    /**
     * Returns an array of filtered domain list file.
     *
     * @param integer $length based upon $valid_toxic_ages
     * @return array
     */
    public function filtered(int $length = null){
        
        // Get our file.
        $file = $this->filtered_raw($length);

        // Parser.
        $file = $this->parser($file);

        // Return.
        return $file;

    }


    /**
     * Returns the whole list in a raw format.
     *
     * @return string
     */
    public function whole_raw(){

        
        // Get our File.
        $file = $this->get_wrapper('toxic_domains_whole.txt');

        return $file;

    }

    /**
     * Returns the whole domain list in an array.
     *
     * @return array
     */
    public function whole(){

        // Get our file.
        $file = $this->whole_raw();

        // Parser.
        $file = $this->parser($file);

        // Return.
        return $file;

    }

    /**
     * returns the partial domain list in a raw format.
     *
     * @return string
     */
    public function partial_raw(){

        // Get our File.
        $file = $this->get_wrapper('toxic_domains_partial.txt');

        return $file;

    }

    /**
     * returns the partial domain list in an array format.
     *
     * @return array
     */
    public function partial(){

        // Get our file.
        $file = $this->partial_raw();

        // Parser.
        $file = $this->parser($file);

        // Return.
        return $file;

    }

}